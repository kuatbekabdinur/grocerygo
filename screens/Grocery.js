import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import Title from './components/Title';
import TodoList from './components/TodoList';

const list = [
  {
    id: 1,
    checked: false,
    name: 'Bread',
  },
  {
    id: 2,
    checked: true,
    name: 'Eggs',
  },
  {
    id: 3,
    checked: false,
    name: 'Yogurt',
  },
  {
    id: 4,
    checked: true,
    name: 'Milk',
  },
  {
    id: 5,
    checked: false,
    name: 'Cheese',
  },
  {
    id: 6,
    checked: false,
    name: 'Butter',
  },
  {
    id: 7,
    checked: true,
    name: 'Vegetable oil',
  },
  {
    id: 8,
    checked: false,
    name: 'Chicken',
  },
];

const Grocery = () => {
  return (
    <ScrollView>
      <View style={styles.view}>
        <Title text="Grocery List" />
        <TodoList list={list} />
        <TodoList list={list} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    padding: 20,
  },
});

export default Grocery;
