import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import Title from './components/Title';

const Cart = () => {
  return (
    <ScrollView>
      <View style={styles.view}>
        <Title text="Cart" />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    padding: 20,
  },
});

export default Cart;
