import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import CheckBox from '@react-native-community/checkbox';

const TodoList = ({list}) => {
  const [toggleCheckBox, setToggleCheckBox] = React.useState(false);
  const data = list.map((l) => (
    <View style={styles.row} key={l.id}>
      <CheckBox
        disabled={false}
        value={l.checked}
        tintColors={{true: '#FF6645', false: '#FF6645'}}
        onValueChange={(newValue) => setToggleCheckBox(newValue)}
      />
      <Text>{l.name}</Text>
    </View>
  ));
  return (
    <View style={styles.view}>
      <Text style={styles.title}>November 1</Text>
      {data}
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    padding: 16,
    marginTop: 16,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 17,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
});

export default TodoList;
