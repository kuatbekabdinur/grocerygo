import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Store = ({text, subtitle}) => {
  return (
    <View style={styles.store}>
      <View style={styles.storeImg}>
        <View style={styles.storeImgView}>
          <Text style={styles.subtitle}>{subtitle}</Text>
        </View>
      </View>
      <View style={styles.storeView}>
        <Text style={styles.storeText}>{text}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  store: {
    height: 180,
    width: 160,
    marginRight: 15,
  },
  storeImg: {
    flex: 2,
  },
  storeImgView: {
    flex: 1,
    backgroundColor: '#FF6645',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  storeView: {
    flex: 1,
    paddingTop: 10,
  },
  storeText: {
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default Store;
