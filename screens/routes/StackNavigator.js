import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../Home';
import Login from '../Login';
import Register from '../Register';
import SplashScreen from '../Splash';
import Scan from '../Screen';

const Stack = createStackNavigator();

const MainStackNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{header: () => null}}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Splash" component={SplashScreen} />
      <Stack.Screen name="Scan" component={Scan} />
    </Stack.Navigator>
  );
};

export {MainStackNavigator};
