import React, {useState} from 'react';
import {
  TextInput,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Formik} from 'formik';
import CheckBox from '@react-native-community/checkbox';

const Register = ({navigation}) => {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  return (
    <View style={styles.view}>
      <Text style={styles.title}>Sign Up</Text>
      <Formik
        initialValues={{
          name: '',
          surname: '',
          email: '',
          phone: '',
          password: '',
        }}
        onSubmit={(values) => navigation.navigate('Login')}>
        {({handleChange, handleBlur, handleSubmit, values}) => (
          <View style={styles.home}>
            <TextInput
              placeholder="First Name"
              style={styles.field}
              onChangeText={handleChange('name')}
              onBlur={handleBlur('name')}
              value={values.name}
              blurOnSubmit
            />
            <TextInput
              placeholder="Last Name"
              style={styles.field}
              onChangeText={handleChange('surname')}
              onBlur={handleBlur('surname')}
              value={values.surname}
              blurOnSubmit
            />
            <TextInput
              placeholder="Email"
              style={styles.field}
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              value={values.email}
              blurOnSubmit
            />
            <TextInput
              placeholder="Phone Number"
              style={styles.field}
              onChangeText={handleChange('phone')}
              onBlur={handleBlur('phone')}
              value={values.phone}
              blurOnSubmit
            />
            <TextInput
              placeholder="Password"
              style={styles.field}
              onChangeText={handleChange('password')}
              onBlur={handleBlur('password')}
              value={values.password}
              secureTextEntry={true}
            />
            <TextInput
              placeholder="Repeat Password"
              style={styles.password}
              onChangeText={handleChange('password2')}
              onBlur={handleBlur('password2')}
              value={values.password2}
              secureTextEntry={true}
            />
            <View style={styles.terms}>
              <CheckBox
                disabled={false}
                value={toggleCheckBox}
                tintColors={'#FF6645'}
                tintColor={'#9E663C'}
                onValueChange={(newValue) => setToggleCheckBox(newValue)}
              />
              <Text style={styles.privacy}>
                Agree with
                <Text
                  style={styles.text}
                  onPress={() => navigation.navigate('Register')}>
                  {' '}
                  Terms & Conditions
                </Text>
              </Text>
            </View>
            <TouchableOpacity style={styles.button} onPress={handleSubmit}>
              <Text style={styles.btnText}>SIGN UP</Text>
            </TouchableOpacity>
          </View>
        )}
      </Formik>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    paddingTop: 16,
    paddingHorizontal: 24,
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 36,
    fontStyle: 'normal',
    color: '#FF6645',
    fontFamily: 'Lato',
    fontWeight: '700',
    lineHeight: 43,
    marginBottom: 15,
  },
  field: {
    height: 40,
    padding: 2,
    fontSize: 18,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    marginBottom: 40,
  },
  password: {
    height: 40,
    padding: 2,
    fontSize: 18,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  privacy: {
    fontSize: 18,
    textAlign: 'center',
  },
  terms: {
    marginTop: 20,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
  },
  text: {
    fontSize: 18,
    textAlign: 'center',
    color: '#FF6645',
    fontWeight: '400',
  },
  button: {
    marginTop: 15,
    paddingTop: 14,
    paddingBottom: 14,
    borderColor: '#FF6645',
    borderWidth: 1,
    borderRadius: 10,
  },
  btnText: {
    fontSize: 18,
    textAlign: 'center',
    color: '#FF6645',
    fontWeight: 'bold',
    lineHeight: 22,
  },
});

export default Register;
