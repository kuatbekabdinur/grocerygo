import * as React from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';
import Title from './components/Title';
import Store from './components/Store';
import Coupon from './components/Coupon';
import TodoList from './components/TodoList';

const list = [
  {
    id: 1,
    checked: false,
    name: 'Bread',
  },
  {
    id: 2,
    checked: true,
    name: 'Eggs',
  },
  {
    id: 3,
    checked: false,
    name: 'Yogurt',
  },
  {
    id: 4,
    checked: true,
    name: 'Milk',
  },
  {
    id: 5,
    checked: false,
    name: 'Cheese',
  },
  {
    id: 6,
    checked: false,
    name: 'Butter',
  },
  {
    id: 7,
    checked: true,
    name: 'Vegetable oil',
  },
  {
    id: 8,
    checked: false,
    name: 'Chicken',
  },
];

const Discover = () => {
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.view}>
        <Title text="Stores" />
        <View style={styles.stores}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <Store text="GroceryGo Store" subtitle="GROCERYGO" />
            <Store text="John’s Store" subtitle="John’s Store" />
            <Store text="Amazon Store" subtitle="Amazon" />
            <Store text="Express Store" subtitle="Express" />
          </ScrollView>
        </View>
        <Title text="Sales and Coupons" />
        <View style={styles.stores}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <Coupon
              title="GROCERYGO"
              subtitle="Get 20% SALE for first purchase"
            />
            <Coupon title="John’s Store" subtitle="10% Off for November" />
            <Coupon title="Amazon" subtitle="10% Off for November" />
            <Coupon title="Express" subtitle="10% Off for November" />
          </ScrollView>
        </View>
        <Title text="Latest Shopping" />
        <TodoList list={list} />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    padding: 20,
  },
  stores: {
    height: 180,
    marginTop: 20,
  },
});

export default Discover;
