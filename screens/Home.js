import * as React from 'react';
import Discover from './Discover';
import Cart from './Cart';
import QR from './QR';
import Grocery from './Grocery';
import Account from './Account';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

const Home = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          if (route.name === 'Discover') {
            iconName = focused ? 'ios-home' : 'ios-home-outline';
          } else if (route.name === 'Cart') {
            iconName = focused ? 'ios-cart' : 'ios-cart-outline';
          } else if (route.name === 'QR') {
            iconName = focused ? 'qr-code' : 'qr-code-outline';
          } else if (route.name === 'Grocery') {
            iconName = focused ? 'list' : 'list-outline';
          } else if (route.name === 'Account') {
            iconName = focused ? 'person' : 'person-outline';
          }

          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      }}>
      <Tab.Screen name="Discover" component={Discover} />
      <Tab.Screen name="Cart" component={Cart} />
      <Tab.Screen name="QR" component={QR} />
      <Tab.Screen name="Grocery" component={Grocery} />
      <Tab.Screen name="Account" component={Account} />
    </Tab.Navigator>
  );
};

export default Home;
