import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {RNCamera} from 'react-native-camera';

const QR = () => {
  return (
    <View style={styles.view}>
      <Text style={styles.title}>GROCERYGO</Text>
      <Ionicons style={styles.qr} name="qr-code-outline" size={250} />
      <Text style={styles.subtitle}>Scan key to enter</Text>
      <TouchableOpacity style={styles.button}>
        <Text style={styles.btnText}>Scan product</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
  },
  title: {
    marginTop: 36,
    fontSize: 36,
    fontWeight: 'bold',
    color: '#FF6645',
  },
  qr: {
    marginTop: 70,
  },
  subtitle: {
    fontSize: 14,
    textTransform: 'uppercase',
  },
  button: {
    marginTop: 50,
    paddingTop: 14,
    paddingBottom: 14,
    paddingLeft: 94,
    paddingRight: 94,
    borderColor: '#FF6645',
    borderWidth: 1,
    borderRadius: 10,
  },
  btnText: {
    fontSize: 18,
    textAlign: 'center',
    color: '#FF6645',
    fontWeight: 'bold',
    lineHeight: 22,
    textTransform: 'uppercase',
  },
});

export default QR;
